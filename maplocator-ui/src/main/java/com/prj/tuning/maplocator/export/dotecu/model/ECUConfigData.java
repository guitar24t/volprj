package com.prj.tuning.maplocator.export.dotecu.model;

public class ECUConfigData {
	public String version;
	public String connect;
	public String communicate;
	public String logSpeed;
	public String hwNumber;
	public String swNumber;
	public String partNumber;
	public String swVersion;
	public String engineId;
	public String lang;
}